﻿using System.Web.Mvc;

public class CustomViewEngine : RazorViewEngine
{
    public CustomViewEngine()
    {
        var viewLocations = new[] {
            "~/Views/Werewolf/{1}/{0}.cshtml",
            "~/Views/Werewolf/Prompts/{0}.cshtml",
        };

        this.PartialViewLocationFormats = viewLocations;
        this.ViewLocationFormats = viewLocations;
    }
}