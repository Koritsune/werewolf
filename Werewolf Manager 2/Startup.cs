﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Werewolf_Manager_2.Startup))]
namespace Werewolf_Manager_2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
