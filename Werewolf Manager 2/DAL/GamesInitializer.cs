﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels;

namespace Werewolf_Manager_2.DAL
{
    public class GamesInitializer : DropCreateDatabaseIfModelChanges<GameDBContext>
    {
        protected override void Seed (GameDBContext context)
        {
            context.Players.Add(new Player()
            {
                Id = Guid.Empty,
                GameId = null,
                Name = null,
                Role = RoleType.Villager
            });

            context.SaveChanges();

            List<Game> games = new List<Game>()
            {
                new Game {GameId = "ER64"},
                new Game {GameId = "ER65"}
            };

            games.ForEach(g => context.Games.Add(g));
            context.SaveChanges();

            List<Player> players = new List<Player>
            {
                new Player {Id = Guid.Parse("3d14e5db-ee43-4703-a404-998c2f3aa011"), GameId = "ER64", Name = "Paul Montgomery", Role = RoleType.Werewolf_Alpha},
                new Player {Id = Guid.Parse("69d3cf5a-5980-42be-a545-73b6a4e8a31d"), GameId = "ER64", Name = "Mitchell Pacifico", Role = RoleType.Werewolf},
                new Player {Id = Guid.Parse("25514083-1ebe-4ec1-a7e2-44f27e65b7ab"), GameId = "ER64", Name = "Amanda Boatswain", Role = RoleType.Cupid},
                new Player {Id = Guid.Parse("6bde8387-8180-4a08-aef9-f0eaa3ab393c"), GameId = "ER64", Name = "Mark Chang", Role = RoleType.Witch_Doctor},
                new Player {Id = Guid.Parse("c08b8388-e0ea-4d66-a3c5-c636f214bc6c"), GameId = "ER64", Name = "Ryan Rosen", Role = RoleType.Survivor },
                new Player {Id = Guid.Parse("3a63e65d-2e57-4f37-8b37-46afbfd13d6e"), GameId = "ER64", Name = "Aaron Chen", Role = RoleType.Hunter },
                new Player {Id = Guid.Parse("4c58313f-ce08-42ab-9c18-8b5eb61e13d8"), GameId = "ER64", Name = "Anna Boatswain", Role = RoleType.Lycanthrope },
                new Player {Id = Guid.Parse("ba29cb7f-ba7e-4189-84f1-db8846baa0e4"), GameId = "ER64", Name = "Nicholas Phan", Role = RoleType.Little_Girl },
                new Player {Id = Guid.Parse("c7f3d1f7-cc3a-435b-9d69-438607059b95"), GameId = "ER64", Name = "Alexandre Simard", Role = RoleType.Actor },
                new Player {Id = Guid.Parse("f1bd047e-002d-4213-8620-0ae76e3fdbf4"), GameId = "ER64", Name = "Matt Arsenault", Role = RoleType.Seer },
                new Player {Id = Guid.Parse("deedae98-0fda-46d7-a97f-1edec609237e"), GameId = "ER64", Name = "Marlon Needlemen", Role = RoleType.Judge },
                new Player {Id = Guid.Parse("b496ae0a-548a-4178-8846-c7f70f01ad72"), GameId = "ER64", Name = "Chris Butt", Role = RoleType.Mason },
                new Player {Id = Guid.Parse("1db87d83-4a73-495b-838f-20aa5df924a0"), GameId = "ER64", Name = "Amanda Melodie", Role = RoleType.Werewolf_Black },
                new Player {Id = Guid.Parse("6c634bfd-8cbc-4cde-8484-f3bf8ef889e9"), GameId = "ER64", Name = "Myriam Belley", Role = RoleType.Jester },
                new Player {Id = Guid.Parse("0dae831b-2625-46bd-8f09-9b48489e1a6b"), GameId = "ER64", Name = "Isabelle Tawa", Role = RoleType.Serial_Killer },
                new Player {Id = Guid.Parse("cbdb231b-26bb-40be-b5a1-4cb4b65046c3"), GameId = "ER64", Name = "Jordan Orsini", Role = RoleType.Beast_Tamer },
            };

            players.ForEach(p => context.Players.Add(p));
            context.SaveChanges();

            for (int count = 0; count < players.Count; count++)
            {
                int rightNeighborIndex = (count == players.Count - 1) ? 0 : count + 1;

                Player player = context.Players.Find(players[count].Id);
                player.Player_RightId = players[rightNeighborIndex].Id;
                context.Entry(player).State = EntityState.Modified;
            }

            context.SaveChanges();

            List<Player> players2 = new List<Player>
            {
                new Player {Id = Guid.Parse("35d4d69e-b98f-4aff-ac82-aab96dc16f1a"), GameId = "ER65", Name = "Paul Montgomery", Role = RoleType.Actor},
                new Player {Id = Guid.Parse("c70576e0-392d-4950-947b-0c473d76c331"), GameId = "ER65", Name = "Mitchell Pacifico", Role = RoleType.Beast_Tamer},
                new Player {Id = Guid.Parse("12d24141-dd95-431e-b8e8-1104c9523d42"), GameId = "ER65", Name = "Amanda Boatswain", Role = RoleType.Judge},
                new Player {Id = Guid.Parse("7d545523-2d1d-46ef-b9bc-54f3c127e956"), GameId = "ER65", Name = "Mark Chang", Role = RoleType.Little_Girl},
                new Player {Id = Guid.Parse("c38239a6-7b44-437b-933b-8eac2358287f"), GameId = "ER65", Name = "Ryan Rosen", Role = RoleType.Lycanthrope },
                new Player {Id = Guid.Parse("34f78292-2e49-4bc5-bf7f-2db6a7fc9a19"), GameId = "ER65", Name = "Aaron Chen", Role = RoleType.Seer },
                new Player {Id = Guid.Parse("8d2e11ed-6607-4b84-8ea2-ba5b45176966"), GameId = "ER65", Name = "Anna Boatswain", Role = RoleType.Survivor },
                new Player {Id = Guid.Parse("9970b967-d64a-4110-8ab5-b6c7784a92f8"), GameId = "ER65", Name = "Nicholas Phan", Role = RoleType.Hunter },
                new Player {Id = Guid.Parse("cf3e816e-ccae-4468-a7df-8017091f2488"), GameId = "ER65", Name = "Alexandre Simard", Role = RoleType.Villager },
                new Player {Id = Guid.Parse("0ff11e89-6c56-41b7-b3e0-b94be9a21bf0"), GameId = "ER65", Name = "Matt Arsenault", Role = RoleType.Werewolf },
            };

            players2.ForEach(p => context.Players.Add(p));
            context.SaveChanges();

            for (int count = 0; count < players2.Count; count++)
            {
                int rightNeighborIndex = (count == players2.Count - 1) ? 0 : count + 1;

                Player player = context.Players.Find(players2[count].Id);
                player.Player_RightId = players2[rightNeighborIndex].Id;
                context.Entry(player).State = EntityState.Modified;
            }

            context.SaveChanges();
        }
    }
}