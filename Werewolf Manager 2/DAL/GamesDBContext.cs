﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Day;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Night;

namespace Werewolf_Manager_2.DAL
{

    public sealed class GameDBContext : DbContext
    {
        [NotMapped]
        private static GameDBContext instance = new GameDBContext();

        [NotMapped]
        private bool isClosed = false;

        public DbSet<Game> Games { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<NightRecord> NightRecords { get; set; }

        public DbSet<DayRecord> DayRecords { get; set; }

        public DbSet<Lovers> Lovers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // sqllocaldb.exe stop MSSQLLocalDB && sqllocaldb.exe delete MSSQLLocalDB

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Player>().HasRequired(p => p.Player_Left).WithMany().HasForeignKey(p => p.Player_LeftId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Player>().HasOptional(p => p.Game).WithMany(g => g.Players).HasForeignKey(p => p.GameId).WillCascadeOnDelete(false);
            modelBuilder.Entity<Lovers>().HasRequired(l => l.Lover1).WithMany().HasForeignKey(l => l.Lover1_Id).WillCascadeOnDelete(false);
            modelBuilder.Entity<Lovers>().HasRequired(l => l.Lover2).WithMany().HasForeignKey(l => l.Lover2_Id).WillCascadeOnDelete(false);
        }

        public Game GetGame(string GameId)
        {
            return Games.Where(g => g.GameId == GameId).FirstOrDefault();
        }

        static GameDBContext()
        {

        }

        private GameDBContext()
        {

        }

        [NotMapped]
        public static GameDBContext Instance
        {
            get
            {
                if (instance.isClosed)
                {
                    instance = new GameDBContext();                    
                }             

                return instance;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            isClosed = true;
        }
    }
}