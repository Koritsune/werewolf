﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels
{
    public class Lovers
    {
        [Key]
        public int Id { get; set; }

        public string GameId { get; set; }

        public Guid Lover1_Id { get; set; }

        public Guid Lover2_Id { get; set; }

        public virtual Player Lover1 { get; set; }

        public virtual Player Lover2 { get; set; }

        public virtual Game Game { get; set; }
    }
}