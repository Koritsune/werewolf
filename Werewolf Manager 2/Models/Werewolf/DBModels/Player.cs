﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Day;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels
{
    [Table("Players")]
    public class Player
    {
        public string GameId { get; set; }

        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool UsedOncePerGamePower { get; set; } = false;

        public RoleType Role { get; set; }

        public Guid Player_LeftId { get; set; }

        public Guid Player_RightId { get; set; }

        public virtual Player Player_Left { get; set; }

        public virtual Player Player_Right { get; set; }

        public virtual Game Game { get; set; }

        public virtual ICollection<NightRecord> NightRecord { get; set; }

        public virtual ICollection<DayRecord> DayRecord { get; set; }

        [NotMapped]
        public bool IsConvertedWerewolf
        {
            get
            {
                if (UserStateManager.GameState != GameState.Setup)
                {
                    return NightRecord.Any(nr => nr.NightState == NightState.Successful_Conversion);
                }

                return false;
            }
            
        }

        [NotMapped]
        public bool Alive
        {
            get
            {
                if (UserStateManager.GameState != GameState.Setup)
                {
                    List<NightRecord> activeNightRecords = NightRecord.Where(nr => nr.NightNumber < UserStateManager.NightNb).ToList();
                    bool killedAtNight = false;

                    //Killed with guaranteed kill
                    killedAtNight = activeNightRecords.Any(nr => nr.NightState.HasFlag(NightState.Super_Killed));
                    //Killed at night and not saved and not converted
                    if (!killedAtNight)
                    {
                        killedAtNight = activeNightRecords.Any(nr => nr.NightState.HasFlag(NightState.Killed) && !nr.NightState.HasFlag(NightState.Saved) && !nr.NightState.HasFlag(NightState.Converted));
                    }

                    bool killedAtDay = false;
                    //Lynched and not saved by the Judge
                    killedAtDay = DayRecord.Any(dr => !dr.DayState.HasFlag(DayState.Judge) && dr.DayState != DayState.Voted);

                    return !(killedAtNight || killedAtDay);
                }

                return true;                
            }
        }
    }
}