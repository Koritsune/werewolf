﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels.Day
{
    public enum DayState
    {
        Lynched,
        Shot,
        Heartbreak,
        Judge,
        Voted
    }
}