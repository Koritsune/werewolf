﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels.Day
{
    public class DayRecord
    {
        [Key]
        public int Id { get; set; }

        public string GameId { get; set; }

        public Guid PlayerId { get; set; }

        public int DayNb { get; set; }

        public DayState DayState { get; set; }

        public virtual Player Player { get; set; }

        public virtual Game Game { get; set; }
    }
}