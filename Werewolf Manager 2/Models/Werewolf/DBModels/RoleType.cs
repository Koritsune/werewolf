﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels
{
    public enum RoleType
    {
        Cupid, // must be first
        Actor,
        Beast_Tamer,
        Survivor,
        Lycanthrope,
        Werewolf,
        Werewolf_Alpha,
        Werewolf_Black,
        Serial_Killer,
        Witch_Doctor,
        Jester,
        Reveal_Conversions,
        Little_Girl,
        Seer,
        Judge,
        Hunter,
        Mason,               
        Villager, //must be last
        /*     
                                    
        */
    }
}