﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels
{
    public enum Team
    {
        None,
        Village,
        Werewolf,
        Jester,
        Executioner,
        Serial_Killer,
        Serial_Killer_Love,
        Occult,
        Love
    }
}