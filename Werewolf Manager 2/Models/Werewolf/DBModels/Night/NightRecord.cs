﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.DBModels;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels.Night
{
    public class NightRecord
    {
        [Key]
        public int Id { get; set; }

        public string GameId { get; set; }

        public Guid PlayerId { get; set; }

        public int NightNumber { get; set; }

        public virtual NightState NightState { get; set; }

        public virtual Player Player { get; set; }

        public virtual Game Game { get; set; }
    }
}