﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.DBModels.Night
{
    [Flags]
    public enum NightState
    {
        None = 0,
        Killed = 1,
        Saved = 1 << 1,
        Super_Killed = 1 << 2,
        Converted = 1 << 3,
        Converted_Save = 1 << 4,
        Successful_Conversion = Killed + Converted,        
    }
}