﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Day;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Night;

namespace Werewolf_Manager_2.Models.Werewolf.DBModels
{
    [Table("Games")]
    public class Game
    {
        [Key]
        public string GameId { get; set; }

        public Guid MayorId { get; set; }

        public Team WinningTeam { get; set; } = Team.None;

        public virtual ICollection<Player> Players { get; set; }

        public virtual ICollection<NightRecord> NightRecords { get; set; }

        public virtual ICollection<DayRecord> DayRecords { get; set; }

        public virtual ICollection<Lovers> Lovers { get; set; }

        public virtual Player Mayor { get; set; }

        [NotMapped]
        [Display(Name = "Total Players")]
        public int TotalPlayers
        {
            get
            {
                if (Players == null)
                    return 0;
                else
                    return Players.Count;
            }
        }
    }
}