﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class YesNoSelectorModel : GenericSelectorModel
    {
        public const string STR_YES = "Yes";

        public const string STR_NO = "No";
    }
}