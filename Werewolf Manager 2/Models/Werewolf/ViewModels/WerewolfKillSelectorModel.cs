﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class WerewolfKillSelectorModel : PlayerSelectorModel
    {
        protected override ICollection<Player> GetSelectablePlayers()
        {
            return db.GetGame(UserStateManager.GameId).Players.Where(p => !GameInfoManager.IsTeamWerewolf(p) && p.Alive).OrderBy(p => p.Name).ToList();
        }

        public override List<SelectListItem> SelectablePlayers
        {
            get
            {
                ICollection<Player> players = GetSelectablePlayers();
                List<SelectListItem> selectionListItems = new List<SelectListItem>();

                selectionListItems.Add(new SelectListItem { Text = "No One", Value = Guid.Empty.ToString() });

                foreach (Player player in players)
                {
                    selectionListItems.Add(new SelectListItem { Text = player.Name, Value = player.Id.ToString() });                    
                }

                return selectionListItems;
            }
        }
    }
}