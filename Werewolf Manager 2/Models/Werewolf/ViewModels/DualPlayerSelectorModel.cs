﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class DualPlayerSelectorModel : PlayerSelectorModel
    {
        public Guid Player2Id { get; set; }

        public string ViewLabel2 { get; set; }
    }
}