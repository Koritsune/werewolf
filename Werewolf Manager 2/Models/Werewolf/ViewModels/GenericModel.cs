﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class GenericModel
    {
        public string ControllerRedirect { get; set; }

        public string ActionRedirect { get; set; }

        public string ViewLabel { get; set; }

        public string ViewTitle { get; set; }
    }
}