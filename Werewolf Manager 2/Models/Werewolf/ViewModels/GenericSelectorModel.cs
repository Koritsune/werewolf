﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.DAL;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class GenericSelectorModel : GenericModel
    {
        protected GameDBContext db {
            get
            {
                return GameDBContext.Instance;
            }
        }
    }
}