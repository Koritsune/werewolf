﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class PlayerViewModel
    {
        protected GameDBContext db
        {
            get
            {
                return GameDBContext.Instance;
            }
        }

        public Guid Id { get; set; } = Guid.Empty;

        public string Name { get; set; }

        public RoleType Role { get; set; }

        [Display(Name = "Right Neighbor")]
        public Guid RightNeighborId { get; set; } = Guid.Empty;

        public List<SelectListItem> SelectablePlayers
        {
            get
            {
                ICollection<Player> players = db.GetGame(UserStateManager.GameId).Players.OrderBy(p => p.Name).ToList();
                List<SelectListItem> selectionListItems = new List<SelectListItem>();

                selectionListItems.Add(new SelectListItem { Text = "No One", Value = "" });

                foreach (Player player in players)
                {
                    if (player.Alive)
                    {
                        selectionListItems.Add(new SelectListItem { Text = player.Name, Value = player.Id.ToString() });
                    }
                }

                return selectionListItems;
            }
        }

        public Player Player
        {
            get
            {
                if (Id == Guid.Empty)
                {
                    Id = Guid.NewGuid();
                }

                Player player = new Player()
                {
                    GameId = UserStateManager.GameId,
                    Name = Name,
                    Id = Id,
                    Role = Role,
                    Player_RightId = RightNeighborId
                };                

                return player;
            }
        }

        public PlayerViewModel(Player player)
        {            
            Name = player.Name;
            Id = player.Id;
            Role = player.Role;
            RightNeighborId = player.Player_RightId == null ? Guid.Empty : player.Player_RightId;            
        }

        public PlayerViewModel()
        {

        }
    }
}