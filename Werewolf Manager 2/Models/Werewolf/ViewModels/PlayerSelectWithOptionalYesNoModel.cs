﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class PlayerSelectWithOptionalYesNoModel : PlayerSelectorAllowNoOneModel
    {
        public bool ShowYesNo { get; set; }

        public string YesNoPromptMessage { get; set; }

        public const string STR_YES = "Yes";

        public const string STR_NO = "No";

        //This is the valueused to determine what was selected, not the displayed string
        public const string STR_SELECT = "No"; 

        public PlayerSelectWithOptionalYesNoModel() : base()
        {

        }

        public PlayerSelectWithOptionalYesNoModel(Player excludedPlayer) : base(excludedPlayer)
        {

        }
    }
}