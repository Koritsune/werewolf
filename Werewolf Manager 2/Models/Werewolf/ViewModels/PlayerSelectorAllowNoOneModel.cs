﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.Models.Werewolf.DBModels;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class PlayerSelectorAllowNoOneModel : PlayerSelectorModel
    {
        public override List<SelectListItem> SelectablePlayers
        {
            get
            {
                ICollection<Player> players = GetSelectablePlayers();
                List<SelectListItem> selectionListItems = new List<SelectListItem>();

                selectionListItems.Add(new SelectListItem { Text = "No One", Value = Guid.Empty.ToString() });

                foreach (Player player in players)
                {
                    if (player.Alive)
                    {
                        selectionListItems.Add(new SelectListItem { Text = player.Name, Value = player.Id.ToString() });
                    }
                }

                return selectionListItems;
            }
        }

        public PlayerSelectorAllowNoOneModel(Player excludedPlayer) : base(excludedPlayer)
        {

        }

        public PlayerSelectorAllowNoOneModel()
        {

        }
    }
}