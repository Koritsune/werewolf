﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;

namespace Werewolf_Manager_2.Models.Werewolf.ViewModels
{
    public class PlayerSelectorModel : GenericSelectorModel
    {
        protected Player ExcludedPlayer;

        public Guid PlayerId { get; set; }  
        
        public PlayerSelectorModel(Player player)
        {
            ExcludedPlayer = player;
        }

        public PlayerSelectorModel()
        {

        }

        protected virtual ICollection<Player> GetSelectablePlayers()
        {
            return db.GetGame(UserStateManager.GameId).Players.Where(p => p != ExcludedPlayer && p.Alive).OrderBy(p => p.Name).ToList();
        }      

        public virtual List<SelectListItem> SelectablePlayers
        {
            get
            {
                ICollection<Player> players = GetSelectablePlayers();
                List<SelectListItem> selectionListItems = new List<SelectListItem>();

                foreach (Player player in players)
                {
                    selectionListItems.Add(new SelectListItem { Text = player.Name, Value = player.Id.ToString() });
                }

                return selectionListItems;
            }                   
        }       
    }
}