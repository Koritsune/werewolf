﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels;

namespace Werewolf_Manager_2.Models.Werewolf.InfoManagers
{
    public static class UserStateManager
    {
        private const string GameIdKey = "GameId";

        private const string DayNbKey = "DayNb";

        private const string NightNbKey = "NightNb";

        private const string SkipPowersNightOneKey = "SkipPowersNightOne";

        private const string CurrentNightRoleKey = "CurrentNightRole";

        private const string GameStateKey = "GameState";

        private const string ExtraDeathsKey = "ExtraDeaths";

        private const string ActorRoleKey = "ActorRole";

        public static string GameId
        {
            get
            {
                return (string)HttpContext.Current.Session[GameIdKey];
            }
            set
            {
                SetKeyInSession(GameIdKey, value);
                SetKeyInSession(GameStateKey, GameState.Setup);
            }
        }

        public static int NightNb
        {
            get
            {
                return (int)HttpContext.Current.Session[NightNbKey];
            }
            set
            {
                SetKeyInSession(NightNbKey, value);
            }
        }

        public static int DayNb
        {
            get
            {
                return (int)HttpContext.Current.Session[DayNbKey];
            }
            set
            {
                SetKeyInSession(DayNbKey, value);
            }
        }        

        public static RoleType CurrentNightRole
        {
            get
            {
                return (RoleType)HttpContext.Current.Session[CurrentNightRoleKey];
            }
            set
            {
                SetKeyInSession(CurrentNightRoleKey, value);
            }
        }

        public static void NextNightRole()
        {
            if (CurrentNightRole == RoleType.Villager)
            {
                CurrentNightRole = RoleType.Cupid;
            }
            else
            {
                CurrentNightRole++;
            }
        }

        public static List<RoleType> ExtraDeaths
        {
            get
            {
                return (List<RoleType>)HttpContext.Current.Session[ExtraDeathsKey];
            }
            set
            {
                SetKeyInSession(ExtraDeathsKey, value);
            }            
            
        }

        public static GameState GameState
        {
            get
            {
                return (GameState)HttpContext.Current.Session[GameStateKey];
            }
            set
            {
                SetKeyInSession(GameStateKey, value);
            }
        }

        private static void SetKeyInSession(string key, object value)
        {
            if (HttpContext.Current.Items.Contains(key))
            {
                HttpContext.Current.Session[key] = value;
            }
            else
            {
                HttpContext.Current.Session.Add(key, value);
            }
        }

        public static RoleType ActorRole
        {
            get
            {
                return (RoleType)HttpContext.Current.Session[ActorRoleKey];
            }
            set
            {
                SetKeyInSession(ActorRoleKey, value);
            }
        }
    }
}