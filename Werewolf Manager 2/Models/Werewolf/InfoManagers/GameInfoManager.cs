﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Night;

namespace Werewolf_Manager_2.Models.Werewolf.InfoManagers
{
    public static class GameInfoManager
    {
        private static GameDBContext db
        {
            get
            {
                return GameDBContext.Instance;
            }
        }

        private static Game Game
        {
            get
            {
                return db.GetGame(UserStateManager.GameId);
            }
        }

        public static readonly RoleType[] WerewolfRolesAlphabetical = new RoleType[]
        {
            RoleType.Actor,
            RoleType.Beast_Tamer,
            RoleType.Cupid,                        
            RoleType.Hunter,
            RoleType.Jester,
            RoleType.Judge,
            RoleType.Little_Girl,
            RoleType.Lycanthrope,
            RoleType.Mason,
            RoleType.Seer,
            RoleType.Serial_Killer,
            RoleType.Survivor,
            RoleType.Villager,
            RoleType.Werewolf,
            RoleType.Werewolf_Alpha,
            RoleType.Werewolf_Black,
            RoleType.Witch_Doctor,
        };

        private static readonly RoleType[] WerewolfRoles = new RoleType[] 
        { 
            RoleType.Werewolf_Alpha,
            RoleType.Werewolf_Black,
            RoleType.Werewolf,
        };

        private static readonly RoleType[] WerewolfConversions = new RoleType[]
        {
            RoleType.Werewolf_Alpha,
            RoleType.Lycanthrope,
            //RoleType.Wild_Child
        };

        private static readonly RoleType[] VillagerRoles = new RoleType[]
        {
            RoleType.Lycanthrope,
            RoleType.Villager,
            RoleType.Cupid,
            RoleType.Actor,
            RoleType.Beast_Tamer,
            RoleType.Witch_Doctor,
            RoleType.Hunter,
            RoleType.Judge,
            RoleType.Mason,
            RoleType.Seer,
            RoleType.Survivor,
            RoleType.Little_Girl,
        };

        private static readonly RoleType[] ActorRoles = new RoleType[]
        {
            RoleType.Beast_Tamer,
            RoleType.Hunter,
            RoleType.Judge,
            RoleType.Little_Girl,
            RoleType.Lycanthrope,
            RoleType.Seer,
            RoleType.Survivor,
            RoleType.Witch_Doctor,           
        };

        private static readonly RoleType[] TriggerDeathRoles = new RoleType[]
        {
            RoleType.Hunter,
        };

        public static int GetTotalWerewolves()
        {            
            return GetCurrentWerewolves().Count;
        }

        public static int GetTotalWerewolfConversions()
        {
            bool isActorAlive = IsRoleStillAlive(RoleType.Actor);
            int totalConversions = 0;

            if (isActorAlive && UserStateManager.GameState != GameState.Setup)
            {
                RoleType actorRole = UserStateManager.ActorRole;

                if (WerewolfConversions.Contains(actorRole))
                {
                    totalConversions++;
                }
            }            

            totalConversions += Game.Players.ToList().Count(r => WerewolfConversions.Contains(r.Role) && r.Alive);

            return totalConversions;
        }

        public static int GetTotalSerialKillers()
        {
            return Game.Players.ToList().Count(r => r.Role == RoleType.Serial_Killer && r.Alive);
        }

        public static int GetTotalAlivePlayers()
        {
            return Game.Players.ToList().Count(p => p.Alive);
        }

        public static int GetTotalJesters()
        {
            return Game.Players.ToList().Count(r => r.Role == RoleType.Jester && r.Alive);
        }

        public static int GetTotalVillagers()
        {
            return Game.Players.ToList().Count(r => VillagerRoles.Contains(r.Role) && r.Alive && !r.IsConvertedWerewolf);
        }

        public static bool IsRoleStillAlive(RoleType role)
        {
            if (role == RoleType.Werewolf)
            {
                if (Game.Players.Any(p => p.IsConvertedWerewolf))
                {
                    return true;
                }
            }

            return Game.Players.ToList().Any(p => p.Role == role && p.Alive);
        }

        public static bool IsTeamWerewolf(Player player)
        {
            return WerewolfRoles.Contains(player.Role) || player.IsConvertedWerewolf;
        }

        public static bool IsTeamVillage(Player player)
        {
            return VillagerRoles.Contains(player.Role) && !player.IsConvertedWerewolf;
        }

        public static Team GetTeam(Player player)
        {
            if (WerewolfRoles.Contains(player.Role) || player.IsConvertedWerewolf)
            {
                return Team.Werewolf;
            }

            if (VillagerRoles.Contains(player.Role))
            {
                return Team.Village;
            }           
            
            if (player.Role == RoleType.Jester)
            {
                return Team.Jester;
            }

            if (player.Role == RoleType.Serial_Killer)
            {
                return Team.Serial_Killer;
            }

            return Team.None;
            //Add more as needed
        }

        public static List<RoleType> TriggersExtraDeath(Player player)
        {
            Game game = db.GetGame(UserStateManager.GameId);
            List<RoleType> ExtraDeaths = new List<RoleType>();

            if (player == null)
            {
                return ExtraDeaths;
            }

            if (TriggerDeathRoles.Contains(player.Role))
            {
                ExtraDeaths.Add(player.Role);
            }

            if(player.Role == RoleType.Actor)
            {
                RoleType actingRole = UserStateManager.ActorRole;
                if (TriggerDeathRoles.Contains(actingRole))
                {
                    ExtraDeaths.Add(RoleType.Actor);
                }
            }

            Lovers lovers = game.Lovers.FirstOrDefault();

            //If player is a lover and the other lover is alive
            if (lovers != null &&
               (lovers.Lover1 == player && lovers.Lover2.Alive
                ||
                lovers.Lover2 == player && lovers.Lover1.Alive))
            {
                ExtraDeaths.Add(RoleType.Cupid);
            }

            return ExtraDeaths;
        }

        public static Player GetLastAttemptedKilledPlayer()
        {
            return Game.NightRecords.Where(nr => nr.NightNumber == UserStateManager.NightNb 
                                                 && nr.NightState.HasFlag(NightState.Killed))
                                    .Select(nr => nr.Player).First();
        }

        public static List<Player> GetTonightsPlayerConversion()
        {
            return Game.NightRecords.Where(nr => nr.NightNumber == UserStateManager.NightNb 
                                                 && nr.NightState == NightState.Successful_Conversion)
                                    .Select(nr => nr.Player).ToList();
        }

        public static List<Player> GetCurrentWerewolves()
        {
            return Game.Players.ToList().Where(p => IsTeamWerewolf(p) && p.Alive).ToList();
        }

        public static RoleType GetLastKilledVillagerForActor()
        {
            int dayNbToCheck = UserStateManager.DayNb;
            int nightNbToCheck = UserStateManager.NightNb;           

            if (UserStateManager.GameState == GameState.Day)
            {
                dayNbToCheck--;
                nightNbToCheck -= 2;
            }
            else
            {
                dayNbToCheck--;
                nightNbToCheck--;
            }  

            var DayRole = Game.DayRecords.ToList().Where(dr => dr.DayNb == dayNbToCheck && dr.Player != null && !dr.Player.Alive)
                                                    .OrderBy(dr => dr.Id)
                                                    .Select(nr => nr.Player.Role)
                                                    .Where(r => ActorRoles.Contains(r))
                                                    .LastOrDefault();
            var NightRole = Game.NightRecords.ToList().Where(nr => nr.NightNumber == nightNbToCheck && !nr.Player.Alive)
                                                      .OrderBy(nr => nr.Id)
                                                      .Select(nr => nr.Player.Role)
                                                      .Where(r => ActorRoles.Contains(r))
                                                      .LastOrDefault();

            if (DayRole == RoleType.Cupid)
            {
                if (NightRole == RoleType.Cupid)
                    return RoleType.Villager;

                return NightRole;
            }
            else
            {
                return DayRole;
            }
        }
    }
}