﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Werewolf_Manager_2.Models.Werewolf.InfoManagers
{
    public enum GameState
    {
        Day,
        Night,
        Finished,
        Setup,
        Started,
        RevealDeaths
    }
}