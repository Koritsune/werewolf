﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels;

namespace Werewolf_Manager_2.Models.Werewolf.InfoManagers
{
    public static class TeamInfoManager
    {
        public class TeamDisplayInfo
        {
            public string TeamName { get; set; }

            public string TeamDescription { get; set; }

            public string VillageLoverDescription { get; set; }

            public string JesterLoverDescription { get; set; }

            public string SerialKillerLoverDescription { get; set; }

            public string WerewolfLoverDescription { get; set; }
        }

        public static Dictionary<Team, TeamDisplayInfo> TeamsInformation { get; } = new Dictionary<Team, TeamDisplayInfo>();

        static TeamInfoManager()
        {
            TeamsInformation.Add(Team.Village, new TeamDisplayInfo()
            {
                TeamName = "Village",
                TeamDescription = "You win if the Serial Killer and Werewolves are dead.",
                VillageLoverDescription = "You win if the village wins and both you and your lover are alive at the end of the game.",
                JesterLoverDescription = "You win if the village wins and both you and your lover are alive at the end of the game.",
                WerewolfLoverDescription = "You win if you and your lover are the only two players alive.",
                SerialKillerLoverDescription = "You win if the serial killer wins and both you and your lover are alive at the end of the game."
            });
            TeamsInformation.Add(Team.Werewolf, new TeamDisplayInfo()
            {
                TeamName = "Werewolfves",
                TeamDescription = "You win if the Serial Killer is dead and you are more than half of all Townspeople currently alive.",
                VillageLoverDescription = "You win if you and your lover are the only two players alive.",
                JesterLoverDescription = "You win if you and your lover are the only two players alive.",
                WerewolfLoverDescription = "You win if the Serial Killer is dead and you are more than half of all Townspeople currently alive, and you and your lover are still alive.",
                SerialKillerLoverDescription = "You win if the Serial Killer wins and both you and your lover are still alive."
            });
            TeamsInformation.Add(Team.Jester, new TeamDisplayInfo()
            {
                TeamName = "Jester",
                TeamDescription = "You win if the town lynches you",
                VillageLoverDescription = "You win if the town lynches you",
                WerewolfLoverDescription = "You win if the town lynches you",
                JesterLoverDescription = "You win if the town lynches you",
                SerialKillerLoverDescription = "You win if the town lynches you"
            });
            TeamsInformation.Add(Team.Serial_Killer, new TeamDisplayInfo()
            {
                TeamName = "Serial Killer",
                TeamDescription = "You win if all members of village are dead. It doesn’t matter how many Werewolves are alive or if the Jester is still alive.",
                VillageLoverDescription = "You win if all members of village are dead and your lover is still alive. It doesn’t matter how many Werewolves are alive or if the Jester is still alive.",
                WerewolfLoverDescription = "You win if all members of village are dead and your lover is still alive. It doesn’t matter how many Werewolves are alive or if the Jester is still alive.",
                SerialKillerLoverDescription = "Not Applicable, there is only one Serial Killer",
                JesterLoverDescription = "You win if all members of village are dead and your lover is still alive. It doesn’t matter how many Werewolves are alive or if the Jester is still alive."
            });
        }
    }
}