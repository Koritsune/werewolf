﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using static Werewolf_Manager_2.Models.Werewolf.InfoManagers.TeamInfoManager;

namespace Werewolf_Manager_2.Models.Werewolf.InfoManagers
{
    public static class RoleInfoManager
    {
        public class RoleDisplayInfo
        {
            public RoleType RoleType { get; set; }

            public string RoleName { get; set; }

            public string RoleDescription { get; set; }

            public string RulingDescription { get; set; }

            public string ImagePath { get; set; }

            public Team Team { get; set; }

            public TeamDisplayInfo TeamDisplayInfo
            {
                get
                {
                    return TeamInfoManager.TeamsInformation[Team];
                }
            }
        }

        public static List<RoleDisplayInfo> RolesInformation { get; } = new List<RoleDisplayInfo>();

        static string imagesPath = @"~/Content/Images/Roles";

        static RoleInfoManager()
        {
            RolesInformation.Add(new RoleDisplayInfo()
            {
               RoleType = RoleType.Actor,
               RoleName = "Actor",
               RoleDescription = "You gain the power of the last killed villager until the next night.", 
               RulingDescription = "If no villager dies during the night or day then you are a villager for the night.",
               ImagePath = $"{imagesPath}/Actor.jpg",
               Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Beast_Tamer,
                RoleName = "Beast Tamer",
                RoleDescription = "If the next live townsperson on your left or right is currently a Werewolf at the beginning of the day there will be a ‘Grr’.",
                RulingDescription = "If the Beast Tamer becomes a Werewolf there will always be a ‘Grr’.",
                ImagePath = $"{imagesPath}/Beast Tamer.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Cupid,
                RoleName = "Cupid",
                RoleDescription = "At the beginning of the game you choose two people to fall in love (they become Team Love), if one of them dies the other dies as well.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Cupid.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Hunter,
                RoleName = "Hunter",
                RoleDescription = "When you are killed (through lynching or murder or heartbreak) you may kill one other townsperson.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Hunter.png",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Jester,
                RoleName = "Jester",
                RoleDescription = "You win if you are lynched. You cannot be converted into a Werewolf.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Jester.jpg",
                Team = Team.Jester
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Judge,
                RoleName = "Judge",
                RoleDescription = "Once per game you may save someone from being lynched after the voting takes place.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Judge.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Little_Girl,
                RoleName = "Little Girl",
                RoleDescription = "You are informed how many werewolves there are at the end of each night (Excluding murders, but including conversions). You cannot be converted to a Werewolf.",
                RulingDescription = "If the Alpha Werewolf uses his ability on you, you are neither killed nor converted.",
                ImagePath = $"{imagesPath}/Little Girl.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Lycanthrope,
                RoleName = "Lycanthrope",
                RoleDescription = "You start the game as a villager, but if you are killed during the night you are reborn as a werewolf.",
                RulingDescription = "If you are saved from death, you are not converted. If the Alpha Werewolf converts you your first ability is negated and you are now a werewolf.",
                ImagePath = $"{imagesPath}/Lycanthrope.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Mason,
                RoleName = "Mason",
                RoleDescription = "At the beginning of the game you wake up and see who your fellow Masons are.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Mason.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Seer,
                RoleName = "Seer",
                RoleDescription = "Once per night you select a townsperson and it is revealed to you if they are currently a villager.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Seer.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Serial_Killer,
                RoleName = "Serial Killer",
                RoleDescription = "Once per night you pick a townsperson to kill. You are also immune to murder.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Serial Killer.jpg",
                Team = Team.Serial_Killer
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Survivor,
                RoleName = "Survivor",
                RoleDescription = "You are immune to murder.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Survivor.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Villager,
                RoleName = "Villager",
                RoleDescription = "Work with the rest of the town to lynch all Werewolves and Serial Killers.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Villager.jpg",
                Team = Team.Village
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Werewolf,
                RoleName = "Werewolf",
                RoleDescription = "During the night you wake up along with your fellow Werewolves and collectively select someone to kill that night.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Werewolf.jpg",
                Team = Team.Werewolf
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Werewolf_Alpha,
                RoleName = "Alpha Werewolf",
                RoleDescription = "During the night you wake up along with your fellow Werewolves and collectively select someone to kill that night. Once per game you may choose to convert the night’s victim.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Alpha Werewolf.jpg",
                Team = Team.Werewolf
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Werewolf_Black,
                RoleName = "Black Werewolf",
                RoleDescription = "During the night you wake up along with your fellow Werewolves and collectively select someone to kill that night. Once per game you may choose to ignore any life saving abilities used on the chosen victim that night.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Black Werewolf.jpg",
                Team = Team.Werewolf
            });
            RolesInformation.Add(new RoleDisplayInfo()
            {
                RoleType = RoleType.Witch_Doctor,
                RoleName = "Witch Doctor",
                RoleDescription = "Every night, you pick one person to blindly protect from death that night. You may not pick the same player twice.",
                RulingDescription = "None",
                ImagePath = $"{imagesPath}/Witch Doctor.jpg",
                Team = Team.Village
            });
        }

    }
}