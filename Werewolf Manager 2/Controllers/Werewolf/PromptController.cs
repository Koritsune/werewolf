﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;
using Werewolf_Manager_2.Models.Werewolf.ViewModels;

namespace Werewolf_Manager_2.Controllers.Werewolf
{
    public class PromptController : Controller
    {
        protected Game game;

        protected bool actorMode = false;

        public PromptController()
        {
            if (UserStateManager.GameId != null)
            {
                game = db.GetGame(UserStateManager.GameId);
            }            
        }

        protected GameDBContext db
        {
            get
            {
                return GameDBContext.Instance;
            }
        }

        protected bool RoleValidFor(RoleType role)
        {
            if (role == RoleType.Werewolf)
            {                
                if (game.Players.Any( p => p.Role == RoleType.Werewolf_Alpha))
                {
                    return true;
                }

                bool isActorAlive = GameInfoManager.IsRoleStillAlive(RoleType.Actor);
                bool isLycanthropeAlive = GameInfoManager.IsRoleStillAlive(RoleType.Lycanthrope);
                bool doesGameHaveLycanthrope = game.Players.Any(p => p.Role == RoleType.Lycanthrope);

                if (isLycanthropeAlive || (!isLycanthropeAlive && isActorAlive))
                {
                    return true;
                }

                return game.Players.Any(p => GameInfoManager.IsTeamWerewolf(p) && p.Alive);
            }
            else if (role == RoleType.Reveal_Conversions)
            {
                return true;
            }
            else if (GameInfoManager.IsRoleStillAlive(role))
            {
                return true;
            }
            else if (role == RoleType.Villager)
            {
                return true;
            }
            else
            {
                return actorMode = GameInfoManager.IsRoleStillAlive(RoleType.Actor) && UserStateManager.ActorRole == role;
            }
        }

        protected bool IsYes()
        {
            if (Request.Form[YesNoSelectorModel.STR_YES] != null)
            {
                return true;
            }
            else if (Request.Form[YesNoSelectorModel.STR_NO] != null)
            {
                return false;
            }

            throw new Exception("An Option was supplied that was not recognized");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}