﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;

namespace Werewolf_Manager_2.Controllers.Werewolf.Setup
{
    public class GamesController : PromptController
    {
        // GET: Games
        public ActionResult Index()
        {
            return View("Index", db.Games.OrderBy(g => g.GameId).Where(g => g.WinningTeam == Team.None).ToList());
        }

        // GET: Games/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            Game game = new Game()
            {
                GameId = StringKeyGenerator.RandomString(4)
            };

            db.Games.Add(game);
            db.SaveChanges();

            UserStateManager.GameId = game.GameId;

            return Index();
        }        

        // GET: Games/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GameId")] Game game)
        {
            if (ModelState.IsValid)
            {
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(game);
        }

        // GET: Games/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Game game = db.Games.Find(id);
            db.Games.Remove(game);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CreateGameWithSamePlayers()
        {
            if (IsYes())
            {
                string oldGameId = UserStateManager.GameId;

                Create();

                var playersInOldGame = db.GetGame(oldGameId).Players.ToList();

                for (int count = 0; count < playersInOldGame.Count; count++)
                {
                    Player currentPlayer = playersInOldGame[count];
                    Player newPlayer = new Player()
                    {
                        GameId = UserStateManager.GameId,
                        Id = Guid.NewGuid(),
                        Name = currentPlayer.Name,
                        Role = RoleType.Villager
                    };                    

                    db.Players.Add(newPlayer);
                }

                db.SaveChanges();

                return RedirectToAction("Index", "Players");
            }
            else
            {
                return RedirectToAction("Index", "Games");
            }                       
        }
    }
}
