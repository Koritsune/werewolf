﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;
using Werewolf_Manager_2.Models.Werewolf.ViewModels;

namespace Werewolf_Manager_2.Controllers.Werewolf.Setup
{
    public class PlayersController : PromptController
    {
        // GET: Players
        public ActionResult Index(string gameId)
        {
            if (!String.IsNullOrWhiteSpace(gameId))
            {
                UserStateManager.GameId = gameId;
            }                
            else if (string.IsNullOrWhiteSpace(UserStateManager.GameId))
            {
                GamesController gamesController = new GamesController();
                gamesController.Create();
                db.Dispose(); //must be disposed or the db context won't create the proper links
            }

            return View(db.GetGame(UserStateManager.GameId).Players);
        }

        // GET: Players/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // GET: Players/Create
        public ActionResult AssignRolesAndNeighbors()
        {
            List<PlayerViewModel> playerViewModels = new List<PlayerViewModel>();

            foreach (Player player in game.Players)
            {
                playerViewModels.Add(new PlayerViewModel(player));
            }

            return View(playerViewModels);
        }

        // POST: Players/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignRolesAndNeighbors(IList<PlayerViewModel> playerViewModels)
        {
            foreach (PlayerViewModel playerViewModel in playerViewModels)
            {
                Player newPlayerInfo = playerViewModel.Player;

                Player oldPlayer = db.Players.Find(newPlayerInfo.Id);

                oldPlayer.Role = newPlayerInfo.Role;
                oldPlayer.Player_RightId = newPlayerInfo.Player_RightId;

                db.Entry(oldPlayer).State = EntityState.Modified;                
            }

            db.SaveChanges();

            return RedirectToAction("ElectMayor", "Game");
        }

        // GET: Players/Create
        public ActionResult Create()
        {            
            return View(new PlayerViewModel());
        }

        // POST: Players/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Role,LeftNeighborId")] PlayerViewModel playerViewModel)
        {
            Player player = playerViewModel.Player;

            if (ModelState.IsValid)
            {
                db.Players.Add(player);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(player);
        }

        // GET: Players/Edit/5
        public ActionResult Edit(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(new PlayerViewModel(player));
        }

        // POST: Players/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Role,LeftNeighborId")] PlayerViewModel playerViewModel)
        {
            //Player is remvoed and added because the Id of the player may change
            db.Entry(playerViewModel.Player).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Players/Delete/5
        public ActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Player player = db.Players.Find(id);
            db.Players.Remove(player);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
