﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;

namespace Werewolf_Manager_2.Controllers.Werewolf.Setup
{
    public class RoleInfoController : Controller
    {
        // GET: RoleInfo
        public ActionResult Index()
        {
            return View(RoleInfoManager.RolesInformation);
        }

        public ActionResult Details(RoleType role)
        {
            return View(RoleInfoManager.RolesInformation.Where(rDI => rDI.RoleType == role).FirstOrDefault());
        }
    }
}