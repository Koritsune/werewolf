﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;
using Werewolf_Manager_2.Models.Werewolf.ViewModels;

namespace Werewolf_Manager_2.Controllers.Werewolf.Gameplay
{
    public class NightController : PromptController
    {
        // GET: Night
        public ActionResult ActivateNightRoles()
        {
            RoleType currentRole = UserStateManager.CurrentNightRole;
            UserStateManager.NextNightRole();
            
            if (currentRole == RoleType.Villager)
            {
                UserStateManager.NightNb++;
                UserStateManager.GameState = GameState.Day;
                return RedirectToAction("CheckForWinner", "Game");
            }

            if (RoleValidFor(currentRole))
            {
                switch (currentRole)
                {
                    case RoleType.Lycanthrope:
                        return SetLycanthropeConversion();
                    case RoleType.Actor:
                        return RevealActorRole();
                    case RoleType.Jester:
                        return SaveJesterFromConversion();
                    case RoleType.Serial_Killer:
                        return SerialKillerKill();
                    case RoleType.Werewolf_Black:
                        return GetBlackWerewolfChoice();
                    case RoleType.Survivor:
                        return SaveSurvivors();
                    case RoleType.Seer:
                        return GetSeerChoice();
                    case RoleType.Werewolf:
                        return WerewolfKill();
                    case RoleType.Werewolf_Alpha:
                        return GetAlphaWerewolfChoice();
                    case RoleType.Reveal_Conversions:
                        return RevealConversions();
                    case RoleType.Little_Girl:
                        return ActivateLittleGirl();
                    case RoleType.Witch_Doctor:
                        return WitchDoctorSave();                    
                    default:
                        return ActivateNightRoles();
                }
            }
            else
            {
                return ActivateNightRoles();
            }                  
        }

        public ActionResult SetupNight()
        {
            Player littleGirl = game.Players.Where(p => p.Role == ((actorMode) ? RoleType.Actor : RoleType.Little_Girl)).First();

            AddNightRecord(littleGirl.Id, NightState.Converted_Save);

            return ActivateNightRoles();
        }

        public ActionResult RevealActorRole()
        {
            RoleType actingRole = GameInfoManager.GetLastKilledVillagerForActor();

            UserStateManager.ActorRole = actingRole;

            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "Night",
                ActionRedirect = "ActivateNightRoles",
                ViewTitle = "Actor",
                ViewLabel = $"The Actor is a {actingRole} until the next night"
            };

            return View("Continue", genericModel);
        }

        public ActionResult SerialKillerKill()
        {
            var currentWerewolves = GameInfoManager.GetCurrentWerewolves();

            Player serialKiller = game.Players.Where(p => p.Role == RoleType.Serial_Killer).First();

            AddNightRecord(serialKiller.Id, NightState.Saved);

            PlayerSelectorAllowNoOneModel playerSelectorModel = new PlayerSelectorAllowNoOneModel(serialKiller)
            {
                ControllerRedirect = "Night",
                ActionRedirect = "SetSerialKillerKill",
                ViewLabel = "Who will the Serial Killer kill tonight?",
                ViewTitle = "Victim"
            };

            return View("PlayerSelect", playerSelectorModel);
        }

        public ActionResult SetSerialKillerKill([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            AddNightRecord(playerSelectorModel.PlayerId, NightState.Killed);

            return ActivateNightRoles();
        }

        public ActionResult SaveJesterFromConversion()
        {
            Player jester = game.Players.Where(p => p.Role == RoleType.Jester).First();

            AddNightRecord(jester.Id, NightState.Converted_Save);

            return ActivateNightRoles();
        }

        public ActionResult GetBlackWerewolfChoice()
        {
            Player blackWerewolf = game.Players.Where(p => p.Role == RoleType.Werewolf_Black).First();

            if (!blackWerewolf.UsedOncePerGamePower)
            {
                YesNoSelectorModel yesNoSelectorModel = new YesNoSelectorModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "SetBlackWerewolf",
                    ViewTitle = "Black Werewolf's choice",
                    ViewLabel = "Does the Black Werewolf guarantee kill?"
                };

                return View("YesNoSelect", yesNoSelectorModel);
            }
            else
            {
                GenericModel genericModel = new GenericModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "ActivateNightRoles",
                    ViewTitle = "Black Werewolf's choice",
                    ViewLabel = "Pretend the Black Werewolf is using their ability"
                };

                return View("Continue", genericModel);
            }
        }

        public ActionResult SetBlackWerewolf()
        {
            if (IsYes())
            {
                Player blackWerewolf = game.Players.Where(p => p.Role == RoleType.Werewolf_Black).First();
                blackWerewolf.UsedOncePerGamePower = true;
                db.Entry(blackWerewolf).State = EntityState.Modified;
                db.SaveChanges();

                Player werewolfKill = GameInfoManager.GetLastAttemptedKilledPlayer();

                if (werewolfKill != null)
                {
                    AddNightRecord(werewolfKill.Id, NightState.Super_Killed);
                }
            }

            return ActivateNightRoles();
        }

        public ActionResult GetSeerChoice()
        {
            string roleName = (actorMode) ? "Actor" : "Seer";

            Player seer = game.Players.Where(p => p.Role == ((actorMode) ? RoleType.Actor : RoleType.Seer)).First();

            PlayerSelectorModel playerSelectorModel = new PlayerSelectorModel(seer)
            {
                ControllerRedirect = "Night",
                ActionRedirect = "RevealRole",
                ViewTitle = $"Who does the {roleName} investigate",
                ViewLabel = "Suspect"
            };

            return View("PlayerSelect", playerSelectorModel);
        }

        public ActionResult RevealRole([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            Player investigatedPlayer = db.Players.Find(playerSelectorModel.PlayerId);
            bool isVillager = GameInfoManager.IsTeamVillage(investigatedPlayer);

            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "Night",
                ActionRedirect = "ActivateNightRoles",
                ViewTitle = "Investigation Result",
                ViewLabel = (isVillager) ? "Is a villager :)" : "Is not a villager!!!!!!!"
            };

            return View("Continue", genericModel);
        }

        public ActionResult ActivateLittleGirl()
        {
            int currentWerewolfCount = GameInfoManager.GetTotalWerewolves();

            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "Night",
                ActionRedirect = "ActivateNightRoles",
                ViewTitle = (actorMode)?"Actor":"Little Girl",
                ViewLabel = $"There are currently: {currentWerewolfCount} werewolves"
            };

            return View("Continue", genericModel);
        }

        public ActionResult SetLycanthropeConversion()
        {
            Player lycanthrope = game.Players.Where(p => p.Role == ((actorMode)?RoleType.Actor:RoleType.Lycanthrope)).First();

            if (!lycanthrope.IsConvertedWerewolf)
            {
                AddNightRecord(lycanthrope.Id, NightState.Converted);
            }            

            return ActivateNightRoles();
        }

        public ActionResult RevealConversions()
        {
            if(GameInfoManager.GetTotalWerewolfConversions() > 0)
            {
                var converted = GameInfoManager.GetTonightsPlayerConversion();

                string convertedText = (converted.Count == 0) ?
                    "Walk around the room and pretend to tap someone for conversion" :
                    $"Walk around the room and tap the following players for conversion: {String.Join(", ", converted.Select(c => c.Name))}";

                GenericModel genericModel = new GenericModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "ActivateNightRoles",
                    ViewTitle = "Reveal Conversions",
                    ViewLabel = convertedText
                };

                return View("Continue", genericModel);
            }

            return ActivateNightRoles();
        }

        public ActionResult GetAlphaWerewolfChoice()
        {
            Player alphaWerewolf = game.Players.Where(p => p.Role == RoleType.Werewolf_Alpha).First();

            if (!alphaWerewolf.UsedOncePerGamePower)
            {
                YesNoSelectorModel yesNoSelectorModel = new YesNoSelectorModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "SetAlphaWerewolf",
                    ViewTitle = "Alpha's choice",
                    ViewLabel = "Does the Alpha Werewolf Convert?"
                };

                return View("YesNoSelect", yesNoSelectorModel);
            }
            else
            {
                GenericModel genericModel = new GenericModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "ActivateNightRoles",
                    ViewTitle = "Alpha Conversion",
                    ViewLabel = "Pretend the alpha werewolf is using their ability"
                };

                return View("Continue", genericModel);
            }
        }

        public ActionResult SetAlphaWerewolf()
        {
            if (IsYes())
            {
                Player alphaWerewolf = game.Players.Where(p => p.Role == RoleType.Werewolf_Alpha).First();
                alphaWerewolf.UsedOncePerGamePower = true;
                db.Entry(alphaWerewolf).State = EntityState.Modified;
                db.SaveChanges();

                Player werewolfKill = GameInfoManager.GetLastAttemptedKilledPlayer();
                
                if (werewolfKill != null)
                {
                    AddNightRecord(werewolfKill.Id, NightState.Converted);
                }
            }

            return ActivateNightRoles();
        }

        public ActionResult SaveSurvivors()
        {            
            game.Players.Where(p => p.Role == ((actorMode)?RoleType.Actor:RoleType.Survivor)).ToList().ForEach(p => AddNightRecord(p.Id, NightState.Saved));
            return ActivateNightRoles();
        }

        public ActionResult WitchDoctorSave()
        {
            string role = (actorMode) ? "Actor" : "Witch Doctor";

            PlayerSelectorModel genericModel = new PlayerSelectorModel()
            {
                ControllerRedirect = "Night",
                ActionRedirect = "SetWitchDoctorSave",
                ViewTitle = $"Who does the {role} save tonight?",
                ViewLabel = "Patient"
            };

            return View("PlayerSelect", genericModel);
        }

        public ActionResult SetWitchDoctorSave([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            AddNightRecord(playerSelectorModel.PlayerId, NightState.Saved);

            return ActivateNightRoles();
        }

        private void AddNightRecord(Guid playerId, NightState nightState)
        {
            NightRecord nightRecord = game.NightRecords.Where(nr => nr.PlayerId == playerId && nr.NightNumber == UserStateManager.NightNb).FirstOrDefault();

            if (nightRecord == null)
            {
                nightRecord = new NightRecord()
                {
                    GameId = UserStateManager.GameId,
                    PlayerId = playerId,
                    NightNumber = UserStateManager.NightNb,
                    NightState = nightState
                };

                db.NightRecords.Add(nightRecord);
                db.SaveChanges();
            }
            else
            {
                nightRecord.NightState |= nightState;
                db.Entry(nightRecord).State = EntityState.Modified;
                db.SaveChanges();
            }
            
        }        

        public ActionResult WerewolfKill()
        {
            var currentWerewolves = GameInfoManager.GetCurrentWerewolves();

            if (currentWerewolves.Count > 0)
            {
                WerewolfKillSelectorModel playerSelectorModel = new WerewolfKillSelectorModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "SetWerewolfKill",
                    ViewLabel = "Who will the Werewolves kill tonight?",
                    ViewTitle = $"Players: {String.Join(", ", currentWerewolves.Select(p => p.Name))}"
                };

                return View("PlayerSelect", playerSelectorModel);
            }
            else
            {
                GenericModel genericModel = new GenericModel()
                {
                    ControllerRedirect = "Night",
                    ActionRedirect = "ActivateNightRoles",
                    ViewTitle = "No Werewolves",
                    ViewLabel = "Pretend the werewolves are choosing someone to kill"
                };

                return View("Continue", genericModel);
            }
            
        }

        public ActionResult SetWerewolfKill([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            AddNightRecord(playerSelectorModel.PlayerId, NightState.Killed);

            return ActivateNightRoles();
        }
    }
}