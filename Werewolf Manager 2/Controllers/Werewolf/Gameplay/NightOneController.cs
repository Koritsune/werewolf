﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;
using Werewolf_Manager_2.Models.Werewolf.ViewModels;

namespace Werewolf_Manager_2.Controllers.Werewolf.Gameplay
{
    public class NightOneController : PromptController
    {
        // GET: NightOne
        public ActionResult RevealRoles()
        {
            Game game = db.GetGame(UserStateManager.GameId);
            RoleType role = UserStateManager.CurrentNightRole;
            UserStateManager.NextNightRole();

            if (RoleValidFor(role))
            {
                switch (role)
                {
                    case RoleType.Cupid:
                        return ChooseLovers();
                    case RoleType.Werewolf:
                        return RevealWerewolves();
                    case RoleType.Mason:
                        return RevealMasons();
                    case RoleType.Villager:
                        return RedirectToAction("CheckForWinner", "Game");
                    default:
                        return RevealRoles();
                }
            }
            else
            {
                return RevealRoles();
            }           
        }

        public ActionResult RevealMasons()
        {
            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "NightOne",
                ActionRedirect = "RevealRoles",
                ViewLabel = "Masons wakeup and look at each other, then Close your eyes",
                ViewTitle = $"Mason Reveal for game: {UserStateManager.GameId}"
            };

            return View("Continue", genericModel);
        }

        public ActionResult ChooseLovers()
        {
            DualPlayerSelectorModel dualPlayerSelectorModel = new DualPlayerSelectorModel()
            {
                ControllerRedirect = "NightOne",
                ActionRedirect = "SetLovers",
                ViewTitle = "Who will fall in love tonight?",
                ViewLabel = "Lover 1",
                ViewLabel2 = "Lover 2"
            };

            return View("DualPlayerSelect", dualPlayerSelectorModel);
        }

        public ActionResult SetLovers([Bind(Include = "PlayerId,Player2Id")] DualPlayerSelectorModel dualPlayerSelectorModel)
        {
            Lovers lovers = new Lovers()
            {
                GameId = UserStateManager.GameId,
                Lover1_Id = dualPlayerSelectorModel.PlayerId,
                Lover2_Id = dualPlayerSelectorModel.Player2Id,
            };

            db.Lovers.Add(lovers);
            db.SaveChanges();

            lovers.Lover1 = db.Players.Find(lovers.Lover1_Id);
            lovers.Lover2 = db.Players.Find(lovers.Lover2_Id);

            return RevealLovers(lovers);
        }

        public ActionResult RevealLovers(Lovers lovers)
        {
            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "NightOne",
                ActionRedirect = "RevealRoles",
                ViewTitle = "Reveal Lovers",
                ViewLabel = $"Walk around the room and tap {lovers.Lover1.Name} and {lovers.Lover2.Name}"
            };

            return View("Continue", genericModel);
        }

        public ActionResult RevealWerewolves()
        {
            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "NightOne",
                ActionRedirect = "RevealRoles",
                ViewLabel = "Werewolves wakeup and look at each other, then Close your eyes",
                ViewTitle = $"Werewolf Reveal for game: {UserStateManager.GameId}"
            };

            return View("Continue", genericModel);
        }        
    }
}