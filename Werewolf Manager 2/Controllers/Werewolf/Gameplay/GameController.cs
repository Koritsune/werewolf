﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Day;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Night;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;
using Werewolf_Manager_2.Models.Werewolf.ViewModels;

namespace Werewolf_Manager_2.Controllers.Werewolf.Gameplay
{
    public class GameController : PromptController
    {
        [ValidateAntiForgeryToken]
        public ActionResult BeginGame(YesNoSelectorModel yesNoModel)
        {
            UserStateManager.DayNb = 0;
            UserStateManager.NightNb = 0;
            UserStateManager.CurrentNightRole = RoleType.Cupid;
            UserStateManager.ActorRole = RoleType.Villager;
            UserStateManager.GameState = (!IsYes()) ? GameState.Day : GameState.Night;
            UserStateManager.ExtraDeaths = new List<RoleType>();

            return RedirectToAction("RevealRoles", "NightOne");
        }

        // GET: Game
        public ActionResult ElectMayor()
        {
            PlayerSelectorModel selectorModel = new PlayerSelectorModel()
            {
                ViewTitle = $"Mayor Election for Game: {UserStateManager.GameId}",
                ControllerRedirect = "Game",
                ActionRedirect = "SetMayor",
                ViewLabel = "Mayor"
            };

            return View("PlayerSelect", selectorModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetMayor([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            Game game = db.GetGame(UserStateManager.GameId);
            game.MayorId = playerSelectorModel.PlayerId;

            if (ModelState.IsValid)
            {
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();

                //Ask if they want to skip Night 1 Powers
                YesNoSelectorModel yesNoModel = new YesNoSelectorModel
                {
                    ViewTitle = $"First Night for Game: {UserStateManager.GameId}",
                    ControllerRedirect = "Game",
                    ActionRedirect = "BeginGame",
                    ViewLabel = "Does the group want to use Powers on the first night?"
                };

                return View("YesNoSelect", yesNoModel);
            }

            return ElectMayor(); ;
        }        

        public ActionResult CheckForWinner()
        {
            Team winningTeam = GetWinner();

            if (winningTeam != Team.None)
            {
                SaveGameWinner(winningTeam);

                return DisplayWinner(winningTeam);
            }
            else
            {
                if (UserStateManager.GameState == GameState.Day)
                {
                    return RedirectToAction("RevealDeaths", "Day");                    
                }
                else
                {
                    GenericModel genericModel = new GenericModel()
                    {
                        ControllerRedirect = "Night",
                        ActionRedirect = "SetupNight",
                        ViewTitle = "Nighttime",
                        ViewLabel = "Everyone in the town goes to sleep"
                    };
                    //Go to Night phase                    
                    return View("Continue", genericModel);
                }
            }            
        }
        
        public Team GetWinner()
        {
            Game game = db.GetGame(UserStateManager.GameId);

            Lovers lovers = game.Lovers.FirstOrDefault();
            bool areLoversOnTheSameTeam = true;
            bool areBothLoversAlive = false;
            bool isOneLoverAJesterAndTheOtherAVillager = false;
            bool isSerialKillerInLove = false;
            bool isVillagerInLove = false;
            bool considerLoveOnlyVictory = false;

            if (lovers != null)
            {
                areLoversOnTheSameTeam = GameInfoManager.GetTeam(lovers.Lover1) == GameInfoManager.GetTeam(lovers.Lover2);
                areBothLoversAlive = lovers.Lover1.Alive && lovers.Lover2.Alive;
                isOneLoverAJesterAndTheOtherAVillager = (lovers.Lover1.Role == RoleType.Jester && GameInfoManager.IsTeamVillage(lovers.Lover2)) || (lovers.Lover2.Role == RoleType.Jester && GameInfoManager.IsTeamVillage(lovers.Lover1));
                isSerialKillerInLove = (lovers.Lover1.Role == RoleType.Serial_Killer || lovers.Lover2.Role == RoleType.Serial_Killer);
                isVillagerInLove = (GameInfoManager.IsTeamVillage(lovers.Lover1) || GameInfoManager.IsTeamVillage(lovers.Lover2));
                considerLoveOnlyVictory = areBothLoversAlive && !areLoversOnTheSameTeam && !isOneLoverAJesterAndTheOtherAVillager;
            }            

            int totalWerewolves = GameInfoManager.GetTotalWerewolves();

            if (game.DayRecords.Any(dr => dr.DayState == DayState.Lynched && dr.Player != null && dr.Player.Role == RoleType.Jester))
            {
                return Team.Jester;
            }
            if (GameInfoManager.GetTotalVillagers() == 0 && GameInfoManager.GetTotalSerialKillers() == 1)
            {
                if (isSerialKillerInLove)
                    return Team.Serial_Killer_Love;
                else
                    return Team.Serial_Killer;
            }
            if (isSerialKillerInLove && isVillagerInLove)
            {
                if (GameInfoManager.GetTotalVillagers() == 1 && GameInfoManager.GetTotalSerialKillers() == 1)
                {
                    return Team.Serial_Killer_Love;
                }
            }
            if (totalWerewolves > GameInfoManager.GetTotalAlivePlayers() - totalWerewolves && GameInfoManager.GetTotalSerialKillers() == 0 && !considerLoveOnlyVictory)
            {
                return Team.Werewolf;
            }
            if (GameInfoManager.GetTotalWerewolves() == 0 && GameInfoManager.GetTotalSerialKillers() == 0 && !considerLoveOnlyVictory)
            {
                return Team.Village;
            }
            if (game.Players.ToList().Where(p => p.Alive).Count() == 2 && considerLoveOnlyVictory)
            {
                return Team.Love;
            }            

            return Team.None;            
        }  
        
        public void SaveGameWinner(Team winningTeam)
        {
            Game game = db.GetGame(UserStateManager.GameId);
            game.WinningTeam = winningTeam;
            db.Entry(game).State = EntityState.Modified;
            db.SaveChanges();
        }

        public ActionResult DisplayWinner(Team winningTeam)
        {
            var winnerMessage = new YesNoSelectorModel()
            {
                ControllerRedirect = "Games",
                ActionRedirect = "CreateGameWithSamePlayers"
            };

            switch (winningTeam)
            {
                case Team.Village:
                    winnerMessage.ViewTitle = "Villager Victory!";
                    winnerMessage.ViewLabel = "The Villagers have won! Play again?";
                    break;
                case Team.Werewolf:
                    winnerMessage.ViewTitle = "Werewolf Victory!";
                    winnerMessage.ViewLabel = "The Werewolves have won! Play again?";
                    break;
                case Team.Love:
                    winnerMessage.ViewTitle = "Love Victory!";
                    winnerMessage.ViewLabel = "True love has beaten all the odds! Isn't it beautiful? Play again?";
                    break;
                case Team.Jester:
                    winnerMessage.ViewTitle = "Jester Victory!";
                    winnerMessage.ViewLabel = "We've made a huge mistake. Play again?";
                    break;
                case Team.Serial_Killer_Love:
                    winnerMessage.ViewTitle = "Serial Killer Love";
                    winnerMessage.ViewLabel = "The Serial Killer murders all but their true love, is murder romantic? Play again?";
                    break;
            }

            return View("YesNoSelect", winnerMessage);
        }
    }
}