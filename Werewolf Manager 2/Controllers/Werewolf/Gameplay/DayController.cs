﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Werewolf_Manager_2.Controllers.Werewolf.Gameplay;
using Werewolf_Manager_2.DAL;
using Werewolf_Manager_2.Models.Werewolf.DBModels;
using Werewolf_Manager_2.Models.Werewolf.DBModels.Day;
using Werewolf_Manager_2.Models.Werewolf.InfoManagers;
using Werewolf_Manager_2.Models.Werewolf.ViewModels;

namespace Werewolf_Manager_2.Controllers.Werewolf.Werewolf.Gameplay
{
    public class DayController : PromptController
    {
        public ActionResult ResolveDayEvents()
        {
            Game game = db.GetGame(UserStateManager.GameId);
            GameController gameController = new GameController();            
            
            if (!game.Mayor.Alive)
            {
                return SelectMayor();
            }
            else if (UserStateManager.ExtraDeaths.Count() > 0)
            {
                RoleType roleKill = UserStateManager.ExtraDeaths.First();
                UserStateManager.ExtraDeaths.RemoveAt(0);
                return ResolveExtraDeath(roleKill);
            }
            //If there is a winner before lynching end the game
            else if (gameController.GetWinner() != Team.None)
            {
                UserStateManager.DayNb++;
                UserStateManager.GameState = GameState.Night;
                return RedirectToAction("CheckForWinner", "Game");
            }
            //If no lynching has occured today
            else if (!game.DayRecords.Any(dr => dr.DayNb == UserStateManager.DayNb && (dr.DayState == DayState.Lynched || dr.DayState == DayState.Judge)))
            {
                return PrepareForLynch();
            }
            //Move to the next night            
            else
            {
                UserStateManager.DayNb++;
                UserStateManager.GameState = GameState.Night;
                return RedirectToAction("CheckForWinner", "Game");
            }                      
        }

        public ActionResult PrepareForLynch()
        {
            if (RoleValidFor(RoleType.Beast_Tamer))
            {
                LinkPlayers();

                Player beastTamer = game.Players.Where(p => p.Role == ((actorMode) ? RoleType.Actor : RoleType.Beast_Tamer)).First();

                string roleName = (actorMode) ? "Actor" : "Beast Tamer";

                if (GameInfoManager.IsTeamWerewolf(beastTamer.Player_Left) || GameInfoManager.IsTeamWerewolf(beastTamer.Player_Right) || GameInfoManager.IsTeamWerewolf(beastTamer))
                {
                    GenericModel genericModel = new GenericModel()
                    {
                        ControllerRedirect = "Day",
                        ActionRedirect = "LynchPlayer",
                        ViewTitle = "RAWWWWWWWR",
                        ViewLabel = $"The Beast growls ({roleName}), a werewolf is nearby!"
                    };

                    return View("Continue", genericModel);
                }
            }

            return LynchPlayer();
        }

        public void CheckForExtraDeaths(Player player)
        {
            var extraDeaths = GameInfoManager.TriggersExtraDeath(player);

            if (extraDeaths.Any())
            {
                extraDeaths.ForEach(d => UserStateManager.ExtraDeaths.Add(d));
            }
        }

        public void CheckForExtraDeaths(IEnumerable<Player> players)
        {
            foreach (Player player in players)
            {
                CheckForExtraDeaths(player);
            }
        }

        public ActionResult RevealDeaths()
        {
            if (UserStateManager.NightNb > 0)
            {
                //Get a list of dead players
                var game = db.GetGame(UserStateManager.GameId);
                var players = db.GetGame(UserStateManager.GameId).NightRecords.Where(nr => nr.NightNumber == UserStateManager.NightNb - 1)
                                                                              .Select(nr => nr.Player).ToList()
                                                                              .Where(p => !p.Alive);

                GenericModel genericModel = new GenericModel()
                {
                    ControllerRedirect = "Day",
                    ActionRedirect = "ResolveDayEvents",
                };

                if (players.Count() == 0)
                {
                    genericModel.ViewTitle = "Daytime";
                    genericModel.ViewLabel = "Everyone in the town wakes up!";
                }
                else
                {
                    CheckForExtraDeaths(players);

                    genericModel.ViewTitle = "Death Announcements";
                    genericModel.ViewLabel = $"The following players have died: {string.Join(", ", players.Select(p => $"{p.Name} ({p.Role})"))}";                    
                }                

                return View("Continue", genericModel);
            }
            else
            {
                return ResolveDayEvents();
            }            
        }

        public ActionResult ResolveExtraDeath(RoleType role)
        {
            switch (role)
            {
                case RoleType.Actor:
                    actorMode = true;
                    return HunterKill();
                case RoleType.Hunter:
                    return HunterKill();
                case RoleType.Cupid:
                    return LoverKill();
                default:
                    return ResolveDayEvents();
            }
        }

        public ActionResult LoverKill()
        {
            Player playerToKill = GetLoverToKill();

            AddDayRecord(playerToKill.Id, DayState.Heartbreak);

            CheckForExtraDeaths(playerToKill);

            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "Day",
                ActionRedirect = "ResolveDayEvents",
                ViewTitle = "Lover Death",
                ViewLabel = $"The following player has died from heartbreak: {playerToKill.Name} ({playerToKill.Role})"
            };

            return View("Continue", genericModel);
        }

        private Player GetLoverToKill()
        {
            Lovers lovers = db.GetGame(UserStateManager.GameId).Lovers.First();

            if (lovers.Lover1.Alive)
            {
                return lovers.Lover1;
            }
            else
            {
                return lovers.Lover2;
            }
        }

        public ActionResult HunterKill()
        {
            string roleName = (actorMode) ? "Actor" : "Hunter";
            PlayerSelectorModel playerSelectorModel = new PlayerSelectorModel()
            {
                ControllerRedirect = "Day",
                ActionRedirect = "SetHunterKill",
                ViewTitle = $"Hunting Victim",
                ViewLabel = $"{roleName}'s Victim"
            };

            return View("PlayerSelect", playerSelectorModel);
        }

        public ActionResult SetHunterKill([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            AddDayRecord(playerSelectorModel.PlayerId, DayState.Shot);

            Player hunterVictim = db.Players.Find(playerSelectorModel.PlayerId);

            CheckForExtraDeaths(hunterVictim);            

            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "Day",
                ActionRedirect = "ResolveDayEvents",
                ViewTitle = "Hunting Victim",
                ViewLabel = $"{hunterVictim.Name} has been shot by a stray bullet, and they were a: {hunterVictim.Role}"
            };

            return View("Continue", genericModel);
        }

        public void AddDayRecord(Guid playerId, DayState dayState)
        {
            DayRecord dayRecord = new DayRecord()
            {
                GameId = UserStateManager.GameId,
                PlayerId = playerId,
                DayState = dayState,
                DayNb = UserStateManager.DayNb
            };

            db.DayRecords.Add(dayRecord);
            db.SaveChanges();
        }

        // GET: Day
        public ActionResult LynchPlayer()
        {
            PlayerSelectWithOptionalYesNoModel playerSelectorModelWithOptionalYesNo = new PlayerSelectWithOptionalYesNoModel()
            {
                ControllerRedirect = "Day",
                ActionRedirect = "SetLynchedPlayer",
                ViewLabel = "Who is Lynched?",
                ViewTitle = "Lynching",
            };

            if (RoleValidFor(RoleType.Judge))
            {
                Player judge = game.Players.Where(p => p.Role == ((actorMode) ? RoleType.Actor : RoleType.Judge)).First();
                string roleName = (actorMode) ? "Actor" : "Judge";

                if (actorMode || !judge.UsedOncePerGamePower)
                {
                    playerSelectorModelWithOptionalYesNo.ShowYesNo = true;
                    playerSelectorModelWithOptionalYesNo.YesNoPromptMessage = $"Does the {roleName} pardon the player?";                   
                }                
            }

            return View("playerSelectWithYesNo", playerSelectorModelWithOptionalYesNo);
        }        

        public ActionResult SetLynchedPlayer([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            AddDayRecord(playerSelectorModel.PlayerId, DayState.Voted);            

            return JudgeSave();
        }

        public ActionResult JudgeSave()
        {
            DayRecord recordToJudge = game.DayRecords.Where(dr => dr.DayNb == UserStateManager.DayNb && dr.DayState == DayState.Voted).First();
            Player votedPlayer = db.Players.Find(recordToJudge.PlayerId);

            GenericModel genericModel = new GenericModel()
            {
                ControllerRedirect = "Day",
                ActionRedirect = "ResolveDayEvents",
            };

            if (IsYes())
            {
                RoleValidFor(RoleType.Judge); //called to set actorMode flag                

                if (!actorMode)
                {
                    Player judge = game.Players.Where(p => p.Role == RoleType.Judge).First();
                    judge.UsedOncePerGamePower = true;
                    db.Entry(judge).State = EntityState.Modified;
                    db.SaveChanges();
                }

                recordToJudge.DayState = DayState.Judge;
                db.Entry(recordToJudge).State = EntityState.Modified;
                db.SaveChanges();

                string roleName = (actorMode) ? "Actor" : "Judge";

                genericModel.ViewTitle = "Political Pardon";
                genericModel.ViewLabel = $"The {roleName} has pardoned {recordToJudge.Player.Name}.";
            }
            else
            {
                recordToJudge.DayState = DayState.Lynched;
                db.Entry(recordToJudge).State = EntityState.Modified;
                db.SaveChanges();

                Player lynchedPlayer = recordToJudge.Player;
                CheckForExtraDeaths(lynchedPlayer);

                genericModel.ViewTitle = "The People have spoken!";
                genericModel.ViewLabel = $"{lynchedPlayer.Name} has been lynched and was a {lynchedPlayer.Role}";        
            }            

            return View("Continue", genericModel);
        }        

        // GET: Game
        public ActionResult SelectMayor()
        {
            PlayerSelectorModel selectorModel = new PlayerSelectorModel()
            {
                ViewTitle = $"Mayor Selection for Game: {UserStateManager.GameId}",
                ControllerRedirect = "Day",
                ActionRedirect = "SetMayor",
                ViewLabel = "Mayor"
            };

            return View("PlayerSelect", selectorModel);
        }

        private Player linkPlayerAndGetRightNeighbor(Player playerToLink)
        {
            Player nextAlivePlayerOnRight = playerToLink.Player_Right;

            while (!nextAlivePlayerOnRight.Alive)
            {
                nextAlivePlayerOnRight = nextAlivePlayerOnRight.Player_Right;
            }

            playerToLink.Player_RightId = nextAlivePlayerOnRight.Id;
            nextAlivePlayerOnRight.Player_LeftId = playerToLink.Id;

            db.Entry(playerToLink).State = EntityState.Modified;
            db.Entry(nextAlivePlayerOnRight).State = EntityState.Modified;
            db.SaveChanges();

            return nextAlivePlayerOnRight;
        }

        private void LinkPlayers()
        {
            Player firstLiveplayer = game.Players.ToList().First(p => p.Alive);
            Player currentPlayerToLink = firstLiveplayer;

            do
            {
                currentPlayerToLink = linkPlayerAndGetRightNeighbor(currentPlayerToLink);
            }
            while (firstLiveplayer.Id != currentPlayerToLink.Id) ;            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetMayor([Bind(Include = "PlayerId")] PlayerSelectorModel playerSelectorModel)
        {
            Game game = db.GetGame(UserStateManager.GameId);
            game.MayorId = playerSelectorModel.PlayerId;

            if (ModelState.IsValid)
            {
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();                
            }

            return ResolveDayEvents();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}